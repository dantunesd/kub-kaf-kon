# RUN

Create a neetwork

```
docker network create minikube --driver bridge --subnet 172.25.0.0/16
```


Start kafka and consumer via docker:

```
docker-compose up
```

---

Create producer image:

```
docker login ...
```

```
docker build -t dantunes/producer:latest .
```

```
docker push dantunes/producer:latest
```

---

Install minikube:

https://minikube.sigs.k8s.io/docs/start/

```
minikube start --network minikube 
```

---

Create a deployment

```
minikube kubectl -- apply -f k8s/deployment.yml
```

----

Apply Service

```
minikube kubectl -- apply -f k8s/service.yml
```

----

Start dashboard or Lens APP

```
minikube dashboard
```

---

Forward port

```
minikube kubectl -- port-forward service/producer 3000:3000
```

---

Produce a message

```
curl -XPOST -d '{"key":"value"}' http://localhost:3000/producer -v
```