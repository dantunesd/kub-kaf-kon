package main

import (
	"log"
	"time"

	"gopkg.in/confluentinc/confluent-kafka-go.v1/kafka"
)

func main() {
	log.Println("starting application")

	consumer, err := kafka.NewConsumer(&kafka.ConfigMap{
		"bootstrap.servers": "broker:29092",
		"client.id":         "consumer-" + time.Now().String(),
		"group.id":          "my-group",
		"socket.max.fails":  2,
	})

	consumer.Subscribe("my-topic", nil)

	if err != nil {
		log.Fatal(err)
	}

	for {
		ev := consumer.Poll(0)
		switch e := ev.(type) {
		case *kafka.Message:
			log.Println(string(e.Value))
		case kafka.Error:
			log.Println("Error: ", e)
			return
		}
	}
}
