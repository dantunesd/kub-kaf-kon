FROM golang:1.17-stretch

WORKDIR /app

COPY . .

ENV APP_PORT=":3000"

RUN go build producer/main.go

CMD [ "./main" ]