package main

import (
	"encoding/json"
	"log"
	"net/http"
	"os"
	"time"

	"gopkg.in/confluentinc/confluent-kafka-go.v1/kafka"
)

func main() {
	handler := http.NewServeMux()
	handler.HandleFunc("/producer", producerHandler)

	log.Println("starting application")
	http.ListenAndServe(os.Getenv("APP_PORT"), handler)
}

func producerHandler(w http.ResponseWriter, r *http.Request) {
	content := map[string]interface{}{}

	if err := json.NewDecoder(r.Body).Decode(&content); err != nil {
		w.WriteHeader(400)
		json.NewEncoder(w).Encode(err.Error())
		return
	}

	if err := produceMessage(content); err != nil {
		w.WriteHeader(500)
		json.NewEncoder(w).Encode(err.Error())
		return
	}

	log.Println("message produced")
	w.WriteHeader(200)
	json.NewEncoder(w).Encode(content)
}

func produceMessage(message map[string]interface{}) error {
	producer, err := kafka.NewProducer(&kafka.ConfigMap{
		"bootstrap.servers":   "broker:29092",
		"client.id":           "producer-" + time.Now().String(),
		"retries":             0,
		"delivery.timeout.ms": 2000,
		"socket.max.fails":    2,
	})

	if err != nil {
		return err
	}

	topic := "my-topic"
	messageConverted, _ := json.Marshal(message)

	deleveryChannel := make(chan kafka.Event, 10000)

	err = producer.Produce(&kafka.Message{
		TopicPartition: kafka.TopicPartition{
			Topic:     &topic,
			Partition: kafka.PartitionAny,
		},
		Value: messageConverted,
	}, deleveryChannel)

	if err != nil {
		return err
	}

	kafkaEvent := <-deleveryChannel
	kafkaMessage := kafkaEvent.(*kafka.Message)

	close(deleveryChannel)

	return kafkaMessage.TopicPartition.Error
}
